package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	m "../models"
	"github.com/gorilla/mux"
)

func CreateTask(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")

	task, err := m.TaskMod.New(userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}
func ChangeTask(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	taskId := mux.Vars(r)["id"]

	task, err := m.TaskMod.Change(taskId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}
func DeleteTask(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	taskId := mux.Vars(r)["id"]

	err := m.TaskMod.Delete(taskId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	w.Write([]byte("Task deleted"))
}

func ChangeSubtask(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	taskId := mux.Vars(r)["id"]

	task, err := m.TaskMod.ChangeSubtask(taskId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}
func DeleteSubtask(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	taskId := mux.Vars(r)["id"]
	task, err := m.TaskMod.DeleteSubtask(taskId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}

func ShowTaskById(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	taskId := mux.Vars(r)["id"]

	task, err := m.TaskMod.FindById(taskId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}
func ShowAllTasksByUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	task, err := m.TaskMod.FindByOwner(userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}
func ShowAllTasksByProject(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	task, err := m.TaskMod.FindByProject(userId, string(body))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(task)
}
