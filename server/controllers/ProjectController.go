package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	m "../models"
	"github.com/gorilla/mux"
)

/* Project */
func CreateProject(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")

	pr, err := m.ProjectMod.New(userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
func ChangeProject(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	projectId := mux.Vars(r)["id"]

	pr, err := m.ProjectMod.Change(projectId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
func DeleteProject(w http.ResponseWriter, r *http.Request) {
	projectId := mux.Vars(r)["id"]
	userId := r.Header.Get("user")
	err := m.ProjectMod.Delete(projectId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	w.Write([]byte("Project deleted"))
}

func AddProjectMember(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	projectId := mux.Vars(r)["id"]

	pr, err := m.ProjectMod.AddMembers(projectId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
func ChangeProjectMember(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	projectId := mux.Vars(r)["id"]

	pr, err := m.ProjectMod.ChangeMembers(projectId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
func DeleteProjectMember(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	projectId := mux.Vars(r)["id"]

	pr, err := m.ProjectMod.DeleteMembers(projectId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
func LeaveProject(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	projectId := mux.Vars(r)["id"]

	err := m.ProjectMod.LeaveProject(projectId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	w.Write([]byte("Group leaved"))
}

func ShowProjectById(w http.ResponseWriter, r *http.Request) {
	projectId := mux.Vars(r)["id"]
	userId := r.Header.Get("user")
	gr, err := m.ProjectMod.FindById(projectId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func ShowProjectsByUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	pr, err := m.ProjectMod.FindAllByUser(userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
func ShowProjectsByGroup(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	pr, err := m.ProjectMod.FindAllByGroup(string(body))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(pr)
}
