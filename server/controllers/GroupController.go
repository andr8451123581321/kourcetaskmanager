package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	m "../models"
	"github.com/gorilla/mux"
)

/* Group */
func CreateGroup(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")

	gr, err := m.GroupMod.New(userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func ChangeGroup(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	groupId := mux.Vars(r)["id"]

	gr, err := m.GroupMod.Change(groupId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func DeleteGroup(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	groupId := mux.Vars(r)["id"]

	err := m.GroupMod.Delete(groupId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.WriteHeader(200)
	w.Write([]byte("Group deleted"))
}

/* Members */
func AddGroupMember(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	groupId := mux.Vars(r)["id"]

	gr, err := m.GroupMod.AddMembers(groupId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func ChangeGroupMember(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	groupId := mux.Vars(r)["id"]

	gr, err := m.GroupMod.ChangeMembers(groupId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func DeleteGroupMember(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	userId := r.Header.Get("user")
	groupId := mux.Vars(r)["id"]

	gr, err := m.GroupMod.DeleteMembers(groupId, userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func LeaveGroup(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	groupId := mux.Vars(r)["id"]

	err := m.GroupMod.LeaveGroup(groupId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	w.Write([]byte("Group leaved"))
}

/* finding */
func ShowGroupById(w http.ResponseWriter, r *http.Request) {
	groupId := mux.Vars(r)["id"]
	userId := r.Header.Get("user")
	gr, err := m.GroupMod.FindById(groupId, userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
func ShowGroupsByUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	gr, err := m.GroupMod.FindAllByUser(userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(gr)
}
