package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	m "../models"
	"github.com/gorilla/mux"
)

func FindUserListByEmail(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u, err := m.UserMod.FindListByEmail(string(body))
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	for i := range u {
		u[i].RemovePass()
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(u)
}
func FindUserById(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]
	u, err := m.UserMod.FindById(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u.RemovePass()
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(u)
}

func FindUserListById(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u, err := m.UserMod.FindListById(body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(u)

}

func FindAllUsers(w http.ResponseWriter, r *http.Request) {
	u, err := m.UserMod.FindAll()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(u)
}
func RegisterUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	u, err := m.UserMod.New(body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	err = m.UserMod.Save(u)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	token, err := m.UserMod.GenAuthToken(u)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u.RemovePass()

	us, _ := json.Marshal(map[string]*m.User{"user": u})
	us[len(us)-1] = ','
	tok, _ := json.Marshal(map[string]string{"token": token})
	js := string(us) + string(tok[1:])

	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("token", token)
	w.WriteHeader(201)
	w.Write([]byte(js))

}
func LoginUser(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u, err := m.UserMod.CheckLoginData(body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	token, err := m.UserMod.GenAuthToken(u)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u.RemovePass()

	us, _ := json.Marshal(map[string]*m.User{"user": u})
	us[len(us)-1] = ','
	tok, _ := json.Marshal(map[string]string{"token": token})
	js := string(us) + string(tok[1:])

	w.Header().Add("Content-Type", "application/json")
	w.Header().Add("token", token)
	w.WriteHeader(201)
	w.Write([]byte(js))
}
func LogoutUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	err := m.UserMod.Logout(userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.WriteHeader(200)
}
func DeleteUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	err := m.UserMod.Delete(userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	w.WriteHeader(200)
	w.Write([]byte("User deleted"))
}
func ShowUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	u, err := m.UserMod.FindById(userId)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u.RemovePass()
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(u)
}
func UpdateUser(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("user")
	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	u, err := m.UserMod.Update(userId, body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	u.RemovePass()
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(201)
	json.NewEncoder(w).Encode(u)
}
