package routers

import (
	c "../controllers"
	mid "../middleware"
	"github.com/gorilla/mux"
)

func AddProjectHandlers(r *mux.Router) {
	r.HandleFunc("/project/create", mid.Auth(c.CreateProject)).Methods("POST")
	r.HandleFunc("/project/{id}", mid.Auth(c.ChangeProject)).Methods("PATCH")
	r.HandleFunc("/project/{id}", mid.Auth(c.DeleteProject)).Methods("DELETE")

	r.HandleFunc("/project/{id}", mid.Auth(c.ShowProjectById)).Methods("GET")
	r.HandleFunc("/project", mid.Auth(c.ShowProjectsByUser)).Methods("GET")
	r.HandleFunc("/project", mid.Auth(c.ShowProjectsByGroup)).Methods("POST")

	r.HandleFunc("/project/{id}/members/add", mid.Auth(c.AddProjectMember)).Methods("PATCH")
	r.HandleFunc("/project/{id}/members/change", mid.Auth(c.ChangeProjectMember)).Methods("PATCH")
	r.HandleFunc("/project/{id}/members/del", mid.Auth(c.DeleteProjectMember)).Methods("PATCH")
	r.HandleFunc("/project/{id}/leave", mid.Auth(c.LeaveProject)).Methods("DELETE")
}
