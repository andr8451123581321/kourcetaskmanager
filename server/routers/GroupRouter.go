package routers

import (
	c "../controllers"
	mid "../middleware"
	"github.com/gorilla/mux"
)

func AddGroupHandlers(r *mux.Router) {
	r.HandleFunc("/group/create", mid.Auth(c.CreateGroup)).Methods("POST")
	r.HandleFunc("/group/{id}", mid.Auth(c.ChangeGroup)).Methods("PATCH")
	r.HandleFunc("/group/{id}", mid.Auth(c.DeleteGroup)).Methods("DELETE")

	r.HandleFunc("/group/{id}", mid.Auth(c.ShowGroupById)).Methods("GET")
	r.HandleFunc("/group", mid.Auth(c.ShowGroupsByUser)).Methods("GET")

	r.HandleFunc("/group/{id}/members/add", mid.Auth(c.AddGroupMember)).Methods("PATCH")
	r.HandleFunc("/group/{id}/members/change", mid.Auth(c.ChangeGroupMember)).Methods("PATCH")
	r.HandleFunc("/group/{id}/members/del", mid.Auth(c.DeleteGroupMember)).Methods("PATCH")
	r.HandleFunc("/group/{id}/leave", mid.Auth(c.LeaveGroup)).Methods("DELETE")
}
