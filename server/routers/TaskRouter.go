package routers

import (
	c "../controllers"
	mid "../middleware"
	"github.com/gorilla/mux"
)

func AddTaskHandlers(r *mux.Router) {
	r.HandleFunc("/task", mid.Auth(c.ShowAllTasksByUser)).Methods("GET")
	r.HandleFunc("/task/{id}", mid.Auth(c.ShowTaskById)).Methods("GET")
	r.HandleFunc("/task", mid.Auth(c.ShowAllTasksByProject)).Methods("POST")

	r.HandleFunc("/task/{id}", mid.Auth(c.DeleteTask)).Methods("DELETE")
	r.HandleFunc("/task/{id}", mid.Auth(c.ChangeTask)).Methods("PATCH")
	r.HandleFunc("/task/create", mid.Auth(c.CreateTask)).Methods("POST")

	r.HandleFunc("/task/{id}/subtask", mid.Auth(c.ChangeSubtask)).Methods("PATCH")
	r.HandleFunc("/task/{id}/delete", mid.Auth(c.DeleteSubtask)).Methods("PATCH")
}
