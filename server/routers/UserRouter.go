package routers

import (
	c "../controllers"
	mid "../middleware"
	"github.com/gorilla/mux"
)

func AddUserHandlers(r *mux.Router) {
	r.HandleFunc("/user/register", c.RegisterUser).Methods("POST")
	r.HandleFunc("/user/list", c.FindAllUsers).Methods("GET")

	r.HandleFunc("/user/email", c.FindUserListByEmail).Methods("POST")
	r.HandleFunc("/user/find", c.FindUserListById).Methods("POST")

	r.HandleFunc("/user/login", c.LoginUser).Methods("POST")
	r.HandleFunc("/user/logout", mid.Auth(c.LogoutUser)).Methods("POST")

	r.HandleFunc("/user/me", mid.Auth(c.DeleteUser)).Methods("DELETE")
	r.HandleFunc("/user/me", mid.Auth(c.ShowUser)).Methods("GET")
	r.HandleFunc("/user/me", mid.Auth(c.UpdateUser)).Methods("PATCH")

	/*r.HandleFunc("/user/view/{id}", c.FindUserById).Methods("GET")
	r.HandleFunc("/user/update/{id}", c.UpdateUserById).Methods("PATCH")
	*/
}
