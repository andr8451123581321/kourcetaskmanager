package routers

import (
	"github.com/gorilla/mux"
)

var router *mux.Router

func New() *mux.Router {
	router = mux.NewRouter()

	AddUserHandlers(router)
	AddGroupHandlers(router)
	AddProjectHandlers(router)
	AddTaskHandlers(router)

	return router
}
