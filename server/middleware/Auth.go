package middleware

import (
	"net/http"
	"strings"

	m "../models"

	jwt "github.com/dgrijalva/jwt-go"
)

func Auth(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req := r.Header.Get("Authorization")
		reqToken := strings.Split(req, " ")
		if len(reqToken) < 2 {
			http.Error(w, "Error token format", 401)
			return
		}

		token, err := jwt.Parse(reqToken[1], func(t *jwt.Token) (interface{}, error) {
			return []byte(m.Secret), nil
		})
		if err == nil && token.Valid {
			claims, _ := token.Claims.(jwt.MapClaims)
			u, err := m.UserMod.FindById(claims["id"].(string))
			if err != nil {
				http.Error(w, err.Error(), 401)
			} else {
				var find bool = false
				for _, value := range u.Tokens {
					if value == reqToken[1] {
						find = true
						break
					}
				}
				if find {
					r.Header.Add("user", u.Id.Hex())
					r.Header.Add("token", reqToken[1])
					next.ServeHTTP(w, r)
				} else {
					http.Error(w, "Please authenticate", 401)
					return
				}
			}
		} else {
			http.Error(w, "Please authenticate", 401)
		}
	})
}
