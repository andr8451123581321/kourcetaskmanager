package models

import (
	"encoding/json"
	"errors"

	"github.com/globalsign/mgo/bson"

	"../db"
)

var groupColl = db.Mongo.C(GroupCollection)

type GroupModel struct{}

type GroupPermits struct {
	AddMembers    bool `bson:"add_members" json:"addMembers"`
	ChangeMembers bool `bson:"change_members" json:"changeMembers"`
	DeleteMembers bool `bson:"delete_members" json:"deleteMembers"`
	ChangeGroup   bool `bson:"change_group" json:"changeGroup"`
	DeleteGroup   bool `bson:"delete_group" json:"deleteGroup"`
	AddProjects   bool `bson:"add_projects" json:"addProjects"`
}

type Group struct {
	Id      bson.ObjectId           `bson:"_id" json:"id"`
	Name    string                  `bson:"name" json:"name"`
	Members map[string]GroupPermits `bson:"members" json:"members"`
	Owner   string                  `bson:"owner" json:"owner"`
}

/* Group adding/deleting/changing */
func (mod *GroupModel) New(userId string, data []byte) (*Group, error) {
	gr := new(Group)
	err := bson.UnmarshalJSON(data, gr)
	if err != nil {
		return nil, err
	}

	if gr.Name == "" {
		return nil, errors.New("Empty group name")
	}
	gr.Id = bson.NewObjectId()
	gr.Owner = userId

	if gr.Members == nil {
		gr.Members = make(map[string]GroupPermits)
	}
	gr.Members[gr.Owner] = GroupAdmin

	err = groupColl.Insert(gr)
	if err != nil {
		return nil, err
	}
	return gr, nil
}
func (mod *GroupModel) Change(groupId string, userId string, data []byte) (*Group, error) {
	gr, err := mod.FindById(groupId, userId)
	if err != nil {
		return nil, err
	}

	if !gr.Members[userId].ChangeGroup {
		return nil, errors.New("No permit to change")
	}

	tmp := new(Group)
	err = bson.UnmarshalJSON(data, tmp)
	if err != nil {
		return nil, err
	}

	if tmp.Name != "" {
		gr.Name = tmp.Name
	}
	if bson.IsObjectIdHex(tmp.Owner) {
		if tmp.Owner != gr.Owner && gr.Owner == userId {
			if _, find := gr.Members[tmp.Owner]; !find {
				return nil, errors.New("User not group member")
			} else {
				gr.Owner = tmp.Owner
			}
		} else if tmp.Owner != gr.Owner {
			return nil, errors.New("Changin owner not permited")
		}

		gr.Members[gr.Owner] = GroupAdmin
	}
	err = groupColl.Update(bson.M{"_id": gr.Id}, gr)
	if err != nil {
		return nil, err
	}

	return gr, nil
}
func (mod *GroupModel) Delete(groupId string, userId string) error {
	gr, err := mod.FindById(groupId, userId)
	if err != nil {
		return err
	}
	if !gr.Members[userId].DeleteGroup {
		return errors.New("No permit to delete")
	}
	pr, err := ProjectMod.FindAllByGroup(gr.Id.Hex())
	if err != nil && err.Error() != "not found" {
		return err
	}

	for _, val := range pr {
		err = ProjectMod.Delete(val.Id.Hex(), val.Owner)
		if err != nil {
			return err
		}
	}

	return groupColl.Remove(bson.M{"_id": gr.Id})
}

/* Members adding/deleting/changing */
func (mod *GroupModel) AddMembers(groupId string, userId string, data []byte) (*Group, error) {
	gr, err := mod.FindById(groupId, userId)
	if err != nil {
		return nil, err
	}
	if !gr.Members[userId].AddMembers {
		return nil, errors.New("No permit to add")
	}

	newMem := make(map[string]GroupPermits)
	err = json.Unmarshal(data, &newMem)
	if err != nil {
		return nil, err
	}

	for key, value := range newMem {
		if !bson.IsObjectIdHex(key) {
			continue
		}
		_, find := gr.Members[key]
		if find {
			continue
		}
		_, err = UserMod.FindById(key)
		if err != nil {
			continue
		}
		gr.Members[key] = value
	}

	err = groupColl.Update(bson.M{"_id": gr.Id}, gr)
	if err != nil {
		return nil, err
	}
	return gr, nil
}
func (mod *GroupModel) DeleteMembers(groupId string, userId string, data []byte) (*Group, error) {
	gr, err := mod.FindById(groupId, userId)
	if err != nil {
		return nil, err
	}
	if !gr.Members[userId].DeleteMembers {
		return nil, errors.New("No permit to delete")
	}

	var users []string
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}

	for _, user := range users {
		if !bson.IsObjectIdHex(user) {
			continue
		}
		_, find := gr.Members[user]
		if !find {
			continue
		}
		if gr.Owner == user {
			continue
		}
		delete(gr.Members, user)
	}

	err = groupColl.Update(bson.M{"_id": gr.Id}, gr)
	if err != nil {
		return nil, err
	}
	return gr, nil
}
func (mod *GroupModel) ChangeMembers(groupId string, userId string, data []byte) (*Group, error) {
	gr, err := mod.FindById(groupId, userId)
	if err != nil {
		return nil, err
	}
	if !gr.Members[userId].ChangeMembers {
		return nil, errors.New("No permit to change")
	}

	mem := make(map[string]GroupPermits)
	err = json.Unmarshal(data, &mem)
	if err != nil {
		return nil, err
	}
	for key, value := range mem {
		if !bson.IsObjectIdHex(key) {
			continue
		}
		_, find := gr.Members[key]
		if !find {
			continue
		}
		if gr.Owner == key {
			continue
		}
		gr.Members[key] = value
	}

	err = groupColl.Update(bson.M{"_id": gr.Id}, gr)
	if err != nil {
		return nil, err
	}
	return gr, nil
}
func (mod *GroupModel) LeaveGroup(groupId string, userId string) error {
	gr, err := mod.FindById(groupId, userId)
	if err != nil {
		return err
	}
	if gr.Owner == userId {
		return errors.New("Can`t delete owner")
	}
	delete(gr.Members, userId)

	return groupColl.Update(bson.M{"_id": gr.Id}, gr)
}

/* Find group/in group */
func (mod *GroupModel) FindById(id string, userId string) (*Group, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, errors.New("Invalid group id")
	}

	var gr = new(Group)
	err := groupColl.Find(bson.M{"_id": bson.ObjectIdHex(id), "members." + userId: bson.M{"$exists": true}}).One(gr)
	if err != nil {
		return nil, err
	}
	return gr, nil
}
func (mod *GroupModel) FindAllByUser(userId string) ([]Group, error) {
	if !bson.IsObjectIdHex(userId) {
		return nil, errors.New("Invalid user id")
	} else {
		var gr []Group
		err := groupColl.Find(bson.M{"members." + userId: bson.M{"$exists": true}}).All(&gr)
		if err != nil {
			return nil, err
		}
		return gr, nil
	}
}
