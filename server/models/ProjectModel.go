package models

import (
	"encoding/json"
	"errors"

	"github.com/globalsign/mgo/bson"

	"../db"
)

var projectColl = db.Mongo.C(ProjectCollection)

type ProjectModel struct{}

type ProjectPermits struct {
	ChangeProject bool `bson:"change_project" json:"changeProject"`
	DeleteProject bool `bson:"delete_project" json:"deleteProject"`
	AddMembers    bool `bson:"add_members" json:"addMembers"`
	ChangeMembers bool `bson:"change_members" json:"changeMembers"`
	DeleteMembers bool `bson:"delete_members" json:"deleteMembers"`
	AddTasks      bool `bson:"add_tasks" json:"addTasks"`
	ChangeTasks   bool `bson:"change_tasks" json:"changeTasks"`
	DeleteTasks   bool `bson:"delete_tasks" json:"deleteTasks"`
}
type Project struct {
	Id      bson.ObjectId             `bson:"_id" json:"id"`
	Name    string                    `bson:"name" json:"name"`
	Members map[string]ProjectPermits `bson:"members" json:"members"`
	Group   string                    `bson:"group" json:"group"`
	Owner   string                    `bson:"owner" json:"owner"`
}

/* project add/change/delete */
func (mod *ProjectModel) New(userId string, data []byte) (*Project, error) {
	pr := new(Project)
	err := bson.UnmarshalJSON(data, pr)
	if err != nil {
		return nil, err
	}
	if pr.Name == "" {
		return nil, errors.New("Empty group name")
	}
	pr.Id = bson.NewObjectId()
	pr.Owner = userId

	if pr.Group == "" {
		if pr.Members == nil {
			pr.Members = make(map[string]ProjectPermits)
		}
		pr.Members[userId] = ProjectAdmin
	} else if bson.IsObjectIdHex(pr.Group) {
		gr, err := GroupMod.FindById(pr.Group, userId)
		if err != nil {
			return nil, err
		}
		if !gr.Members[userId].AddProjects {
			return nil, errors.New("No permit to add project")
		}
		if pr.Members == nil {
			pr.Members = make(map[string]ProjectPermits)
			for key := range gr.Members {
				pr.Members[key] = ProjectDefault
			}
		}
		pr.Members[userId] = ProjectAdmin

	} else {
		return nil, errors.New("Invalid group id")
	}

	err = projectColl.Insert(pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}
func (mod *ProjectModel) Change(projectId string, userId string, data []byte) (*Project, error) {
	if !bson.IsObjectIdHex(projectId) {
		return nil, errors.New("Invalid project id")
	}
	pr, err := mod.FindById(projectId, userId)
	if err != nil {
		return nil, err
	}
	if !pr.Members[userId].ChangeProject {
		return nil, errors.New("No permit to change")
	}
	tmp := new(Project)
	err = bson.UnmarshalJSON(data, tmp)
	if err != nil {
		return nil, err
	}

	if tmp.Name != "" {
		pr.Name = tmp.Name
	}
	if bson.IsObjectIdHex(tmp.Owner) {
		if tmp.Owner != pr.Owner && pr.Owner == userId {
			if _, find := pr.Members[tmp.Owner]; !find {
				return nil, errors.New("User not project member")
			} else {
				pr.Owner = tmp.Owner
			}
		} else if pr.Owner != tmp.Owner {
			return nil, errors.New("Changin owner not permited")
		}

		pr.Members[pr.Owner] = ProjectAdmin
	}
	err = projectColl.Update(bson.M{"_id": pr.Id}, pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}
func (mod *ProjectModel) Delete(projectId string, userId string) error {
	if !bson.IsObjectIdHex(projectId) {
		return errors.New("Invalid project id")
	}
	pr, err := mod.FindById(projectId, userId)
	if err != nil {
		return err
	}
	if !pr.Members[userId].DeleteProject {
		return errors.New("No permit to delete")
	}
	err = projectColl.Remove(bson.M{"_id": pr.Id})
	if err != nil {
		return err
	}
	_, err = taskColl.RemoveAll(bson.M{"project": pr.Id.Hex()})
	if err != nil && err.Error() != "not found" {
		return err
	}
	return nil
}

/* members add/change/delete */
func (mod *ProjectModel) AddMembers(projectId string, userId string, data []byte) (*Project, error) {
	pr, err := mod.FindById(projectId, userId)
	if err != nil {
		return nil, err
	}
	if !pr.Members[userId].AddMembers {
		return nil, errors.New("No permit to add")
	}

	newMem := make(map[string]ProjectPermits)
	err = json.Unmarshal(data, &newMem)
	if err != nil {
		return nil, err
	}

	for key, value := range newMem {
		if !bson.IsObjectIdHex(key) {
			continue
		}
		_, find := pr.Members[key]
		if find {
			continue
		}
		_, err = UserMod.FindById(key)
		if err != nil {
			continue
		}
		pr.Members[key] = value
	}

	err = projectColl.Update(bson.M{"_id": pr.Id}, pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}
func (mod *ProjectModel) DeleteMembers(projectId string, userId string, data []byte) (*Project, error) {
	pr, err := mod.FindById(projectId, userId)
	if err != nil {
		return nil, err
	}
	if !pr.Members[userId].DeleteMembers {
		return nil, errors.New("No permit to delete")
	}

	var users []string
	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, err
	}

	for _, user := range users {
		if !bson.IsObjectIdHex(user) {
			continue
		}
		_, find := pr.Members[user]
		if !find {
			continue
		}
		if pr.Owner == user {
			continue
		}
		delete(pr.Members, user)
	}

	err = projectColl.Update(bson.M{"_id": pr.Id}, pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}
func (mod *ProjectModel) ChangeMembers(projectId string, userId string, data []byte) (*Project, error) {
	pr, err := mod.FindById(projectId, userId)
	if err != nil {
		return nil, err
	}
	if !pr.Members[userId].ChangeMembers {
		return nil, errors.New("No permit to change")
	}

	mem := make(map[string]ProjectPermits)
	err = json.Unmarshal(data, &mem)
	if err != nil {
		return nil, err
	}
	for key, value := range mem {
		if !bson.IsObjectIdHex(key) {
			continue
		}
		_, find := pr.Members[key]
		if !find {
			continue
		}
		if pr.Owner == key {
			continue
		}
		pr.Members[key] = value
	}

	err = projectColl.Update(bson.M{"_id": pr.Id}, pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}
func (mod *ProjectModel) LeaveProject(projectId string, userId string) error {
	pr, err := mod.FindById(projectId, userId)
	if err != nil {
		return err
	}
	if pr.Owner == userId {
		return errors.New("Can`t delete owner")
	}
	delete(pr.Members, userId)

	return projectColl.Update(bson.M{"_id": pr.Id}, pr)
}

/* find projects */
func (mod *ProjectModel) FindById(projectId string, userId string) (*Project, error) {
	if !bson.IsObjectIdHex(projectId) {
		return nil, errors.New("Invalid project id")
	}
	var pr = new(Project)
	err := projectColl.Find(bson.M{"_id": bson.ObjectIdHex(projectId), "members." + userId: bson.M{"$exists": true}}).One(pr)
	if err != nil {
		return nil, err
	}
	return pr, nil
}
func (mod *ProjectModel) FindAllByUser(userId string) ([]Project, error) {
	if !bson.IsObjectIdHex(userId) {
		return nil, errors.New("Invalid user id")
	} else {
		var pr []Project
		err := projectColl.Find(bson.M{"members." + userId: bson.M{"$exists": true}}).All(&pr)
		if err != nil {
			return nil, err
		}
		return pr, nil
	}
}
func (mod *ProjectModel) FindAllByGroup(groupId string) ([]Project, error) {
	if !bson.IsObjectIdHex(groupId) {
		return nil, errors.New("Invalid group id")
	} else {
		var pr []Project
		err := projectColl.Find(bson.M{"group": groupId}).All(&pr)
		if err != nil {
			return nil, err
		}
		return pr, nil
	}
}
