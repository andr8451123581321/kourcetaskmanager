package models

import (
	"encoding/json"
	"errors"
	"net/mail"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/globalsign/mgo/bson"
	"golang.org/x/crypto/bcrypt"

	"../db"
)

var userColl = db.Mongo.C(UserCollection)
var Secret = "$2a$10$ELgUjqUolu61MzSHFwrM0x3CcGNVkBLSLz8LHouVMcaEJIfZHFu"

type UserModel struct{}
type User struct {
	Id       bson.ObjectId `bson:"_id" json:"id"`
	Name     string        `bson:"name" json:"name"`
	Email    string        `bson:"email" json:"email"`
	Password string        `bson:"password" json:"password"`
	Tokens   []string      `bson:"tokens" json:"-"`
}

/* User creation/changing */
func (mod *UserModel) New(data []byte) (*User, error) {
	u := new(User)

	err := bson.UnmarshalJSON(data, u)
	if err != nil {
		return nil, err
	}

	u.Id = bson.NewObjectId()

	u.Tokens = nil

	err = mod.Validate(u)
	if err != nil {
		return nil, err
	}

	return u, nil
}
func (mod *UserModel) Save(u *User) error {
	return userColl.Insert(u)
}
func (mod *UserModel) Validate(u *User) error {
	//email validation
	_, err := mail.ParseAddress(u.Email)
	if err != nil {
		return err
	}
	tmp := new(User)
	err = userColl.Find(bson.M{"email": u.Email}).One(tmp)
	if err == nil && tmp.Id != u.Id {
		return errors.New("Error: email already used")
	} else if err != nil && err.Error() != "not found" {
		return err
	}

	//name validation
	if len(u.Name) < 3 {
		return errors.New("Error: name length")
	}
	//password validation
	if len(u.Password) < 6 {
		return errors.New("Error: password length")
	} else {
		hash, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
		u.Password = string(hash)
	}

	return nil
}
func (mod *UserModel) Update(id string, data []byte) (*User, error) {
	var tmp = new(User)

	u, err := mod.FindById(id)
	if err != nil {
		return nil, err
	}

	err = bson.UnmarshalJSON(data, tmp)
	if err != nil {
		return nil, err
	}

	if tmp.Name != "" {
		u.Name = tmp.Name
	}
	if tmp.Email != "" {
		u.Email = tmp.Email
	}
	if tmp.Password != "" {
		u.Password = tmp.Password
	}
	err = mod.Validate(u)
	if err != nil {
		return nil, err
	}
	err = userColl.Update(bson.M{"_id": bson.ObjectIdHex(id)}, u)
	if err != nil {
		return nil, err
	}
	return u, nil
}
func (mod *UserModel) Delete(id string) error {
	gr, err := GroupMod.FindAllByUser(id)
	if err != nil && err.Error() != "not found" {
		return err
	}
	for _, val := range gr {
		if val.Owner == id {
			GroupMod.Delete(val.Id.Hex(), id)
		} else {
			GroupMod.LeaveGroup(val.Id.Hex(), id)
		}
	}
	pr, err := ProjectMod.FindAllByUser(id)
	if err != nil && err.Error() != "not found" {
		return err
	}
	for _, val := range pr {
		if val.Owner == id {
			ProjectMod.Delete(val.Id.Hex(), id)
		} else {
			ProjectMod.LeaveProject(val.Id.Hex(), id)
		}
	}
	_, err = taskColl.RemoveAll(bson.M{"owner": id})
	if err != nil && err.Error() != "not found" {
		return err
	}
	return userColl.Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

/* User login/logout */
func (mod *UserModel) CheckLoginData(data []byte) (*User, error) {
	var tmp = new(User)
	var u = new(User)

	err := bson.UnmarshalJSON(data, tmp)
	if err != nil {
		return nil, err
	}
	if tmp.Email == "" {
		return nil, errors.New("Empty email")
	}
	if tmp.Password == "" {
		return nil, errors.New("Empty password")
	}
	err = userColl.Find(bson.M{"email": tmp.Email}).One(u)
	if err != nil {
		if err.Error() == "not found" {
			return nil, errors.New("Incorrect email")
		}
		return nil, err
	}
	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(tmp.Password))
	if err != nil {
		if err.Error() == "crypto/bcrypt: hashedPassword is not the hash of the given password" {
			return nil, errors.New("Incorrect password")
		}
		return nil, err
	}

	return u, err
}
func (mod *UserModel) Logout(id string) error {
	u, err := mod.FindById(id)
	if err != nil {
		return err
	}
	u.Tokens = []string{}

	err = userColl.Update(bson.M{"_id": u.Id}, u)
	if err != nil {
		return err
	}
	return nil
}
func (mod *UserModel) GenAuthToken(u *User) (string, error) {
	claims := make(jwt.MapClaims)
	claims["id"] = u.Id
	claims["time"] = time.Now().Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(Secret))
	if err != nil {
		return "", err
	}
	u.Tokens = append(u.Tokens, tokenString)
	err = userColl.Update(bson.M{"_id": u.Id}, u)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

/* Find users */
func (mod *UserModel) FindById(id string) (*User, error) {
	var u = new(User)
	err := userColl.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(u)
	if err != nil {
		return nil, err
	}
	return u, nil
}
func (mod *UserModel) FindListByEmail(email string) ([]User, error) {
	emails := strings.Split(email, " ")
	var u []User
	err := userColl.Find(bson.M{"email": bson.M{"$in": emails}}).All(&u)
	if err != nil {
		return nil, err
	}
	return u, nil
}
func (mod *UserModel) FindAll() ([]User, error) {
	var u []User
	err := userColl.Find(nil).All(&u)
	if err != nil {
		return nil, err
	}

	return u, nil
}
func (mod *UserModel) FindListById(data []byte) ([]User, error) {
	var idList []string
	var u []User
	err := json.Unmarshal(data, &idList)
	if err != nil {
		return nil, err
	}
	for _, id := range idList {
		user, err := mod.FindById(id)
		if err != nil {
			continue
		}
		user.RemovePass()
		u = append(u, *user)
	}
	return u, nil
}

func (u *User) RemovePass() *User {
	u.Password = ""
	return u
}
