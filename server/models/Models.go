package models

const (
	UserCollection    = "users"
	TaskCollection    = "tasks"
	GroupCollection   = "groups"
	ProjectCollection = "projects"
)

var UserMod = new(UserModel)
var GroupMod = new(GroupModel)
var ProjectMod = new(ProjectModel)
var TaskMod = new(TaskModel)

var GroupAdmin = GroupPermits{
	AddMembers:    true,
	ChangeMembers: true,
	DeleteMembers: true,
	ChangeGroup:   true,
	DeleteGroup:   true,
	AddProjects:   true,
}

var ProjectAdmin = ProjectPermits{
	ChangeProject: true,
	DeleteProject: true,
	AddMembers:    true,
	ChangeMembers: true,
	DeleteMembers: true,
	AddTasks:      true,
	ChangeTasks:   true,
	DeleteTasks:   true,
}

var ProjectDefault = ProjectPermits{
	ChangeProject: false,
	DeleteProject: false,
	AddMembers:    false,
	ChangeMembers: false,
	DeleteMembers: false,
	AddTasks:      true,
	ChangeTasks:   true,
	DeleteTasks:   true,
}
