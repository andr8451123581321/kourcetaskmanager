package models

import (
	"encoding/json"
	"errors"

	"github.com/globalsign/mgo/bson"

	"../db"
)

var taskColl = db.Mongo.C(TaskCollection)

type TaskModel struct{}

type Task struct {
	Id        bson.ObjectId   `bson:"_id" json:"id"`
	Desc      string          `bson:"description" json:"description"`
	Completed bool            `bson:"completed" json:"completed"`
	Subtasks  map[string]bool `bson:"subtasks" json:"subtasks"`
	Project   string          `bson:"project" json:"project"`
	Owner     string          `bson:"owner" json:"owner"`
}

/* task create/change/delete */
func (mod *TaskModel) New(userId string, data []byte) (*Task, error) {
	task := new(Task)
	err := bson.UnmarshalJSON(data, task)
	if err != nil {
		return nil, err
	}

	task.Id = bson.NewObjectId()
	if task.Desc == "" {
		return nil, errors.New("Empty task description")
	}
	if task.Project == "" {
		task.Owner = userId
	} else if bson.IsObjectIdHex(task.Project) {
		pr, err := ProjectMod.FindById(task.Project, userId)
		if err != nil {
			return nil, err
		}
		if !pr.Members[userId].AddTasks {
			return nil, errors.New("No permit to add task")
		}
		task.Project = pr.Id.Hex()
		task.Owner = ""
	} else {
		return nil, errors.New("Invalid project id")
	}
	task.Subtasks = make(map[string]bool)

	err = taskColl.Insert(task)
	if err != nil {
		return nil, err
	}

	return task, nil
}
func (mod *TaskModel) Change(taskId string, userId string, data []byte) (*Task, error) {
	task, err := mod.FindById(taskId, userId)
	if err != nil {
		return nil, err
	}
	tmp := new(Task)
	err = bson.UnmarshalJSON(data, tmp)
	if err != nil {
		return nil, err
	}
	if task.Project != "" {
		pr, err := ProjectMod.FindById(task.Project, userId)
		if err != nil {
			return nil, err
		}
		if !pr.Members[userId].ChangeTasks {
			return nil, errors.New("No permit to change")
		}
	}

	if tmp.Desc != "" {
		task.Desc = tmp.Desc
	}
	if tmp.Completed != task.Completed && len(task.Subtasks) == 0 {
		task.Completed = tmp.Completed
	}

	err = taskColl.Update(bson.M{"_id": task.Id}, task)
	if err != nil {
		return nil, err
	}
	return task, nil
}
func (mod *TaskModel) Delete(taskId string, userId string) error {
	task, err := mod.FindById(taskId, userId)
	if err != nil {
		return err
	}
	if task.Project != "" {
		pr, err := ProjectMod.FindById(task.Project, userId)
		if err != nil {
			return err
		}
		if !pr.Members[userId].DeleteTasks {
			return errors.New("No permit to delete")
		}
	}
	return taskColl.Remove(bson.M{"_id": task.Id})
}

/* subtasks add/change/delete */
func (mod *TaskModel) ChangeSubtask(taskId string, userId string, data []byte) (*Task, error) {
	task, err := mod.FindById(taskId, userId)
	if err != nil {
		return nil, err
	}
	if task.Project != "" {
		pr, err := ProjectMod.FindById(task.Project, userId)
		if err != nil {
			return nil, err
		}
		if !pr.Members[userId].ChangeTasks {
			return nil, errors.New("No permit to change")
		}
	}

	tmp := make(map[string]bool)
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}

	task.Completed = true
	for key, value := range tmp {
		if key == "" {
			continue
		}
		task.Subtasks[key] = value
	}
	for _, val := range task.Subtasks {
		if !val {
			task.Completed = val
			break
		}
	}

	err = taskColl.Update(bson.M{"_id": task.Id}, task)
	if err != nil {
		return nil, err
	}
	return task, nil
}
func (mod *TaskModel) DeleteSubtask(taskId string, userId string, data []byte) (*Task, error) {
	task, err := mod.FindById(taskId, userId)
	if err != nil {
		return nil, err
	}
	if task.Project != "" {
		pr, err := ProjectMod.FindById(task.Project, userId)
		if err != nil {
			return nil, err
		}
		if !pr.Members[userId].ChangeTasks {
			return nil, errors.New("No permit to change")
		}
	}

	var tmp []string
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}

	for _, val := range tmp {
		if _, find := task.Subtasks[val]; !find {
			continue
		}
		delete(task.Subtasks, val)
	}
	task.Completed = true
	for _, val := range task.Subtasks {
		if !val {
			task.Completed = val
			break
		}
	}

	err = taskColl.Update(bson.M{"_id": task.Id}, task)
	if err != nil {
		return nil, err
	}
	return task, nil
}

/* find tasks */
func (mod *TaskModel) FindById(taskId string, userId string) (*Task, error) {
	if !bson.IsObjectIdHex(taskId) {
		return nil, errors.New("Invalid task id")
	} else {
		var task = new(Task)
		err := taskColl.Find(bson.M{"_id": bson.ObjectIdHex(taskId)}).One(task)
		if err != nil {
			return nil, err
		}
		if task.Owner != "" {
			if task.Owner != userId {
				return nil, errors.New("Wrong user id")
			}
			return task, nil
		} else {
			_, err := ProjectMod.FindById(task.Project, userId)
			if err != nil {
				return nil, err
			}
			return task, nil
		}
	}
}
func (mod *TaskModel) FindByOwner(userId string) ([]Task, error) {
	if !bson.IsObjectIdHex(userId) {
		return nil, errors.New("Invalid user id")
	}

	var task []Task
	err := taskColl.Find(bson.M{"owner": userId}).All(&task)
	if err != nil {
		return nil, err
	}
	return task, nil
}
func (mod *TaskModel) FindByProject(userId string, projectId string) ([]Task, error) {
	if !bson.IsObjectIdHex(userId) {
		return nil, errors.New("Invalid user id")
	}
	if !bson.IsObjectIdHex(projectId) {
		return nil, errors.New("Invalid project id")
	}

	_, err := ProjectMod.FindById(projectId, userId)
	if err != nil {
		return nil, err
	}
	var task []Task
	err = taskColl.Find(bson.M{"project": projectId}).All(&task)
	if err != nil {
		return nil, err
	}
	return task, nil

}
