package db

import (
	"github.com/globalsign/mgo"
)

const (
	address  = "mongodb://127.0.0.1"
	database = "kource"
)

var Mongo, Session = Connect()

func Connect() (*mgo.Database, *mgo.Session) {
	session, err := mgo.Dial(address)
	if err != nil {
		session.Close()
		panic(err)
	}
	db := session.DB(database)

	return db, session
}
