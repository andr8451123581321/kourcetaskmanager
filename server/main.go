package main

import (
	"net/http"

	"github.com/rs/cors"

	"./db"
	"./routers"
)

func main() {
	r := routers.New()

	corsOpts := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:8080"}, //you service is available and allowed for this base url
		AllowedMethods: []string{
			"GET", //http methods for your app
			"POST",
			"PATCH",
			"DELETE",
		},

		AllowedHeaders: []string{
			"*", //or you can your header key values which you are using in your application

		},
	})

	http.ListenAndServe(":3000", corsOpts.Handler(r))

	defer db.Session.Close()
}
